
// import './App.css';
import Header from "./component/Header"
import Banner from "./component/Banner"
import List from './component/List';
function App() {
  return (
    <div className="App">
        <Header/>
        <Banner/>
        <List/>
    </div>
  );
}

export default App;
